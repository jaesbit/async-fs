const fs = require("fs");
const ignorer = require("ignore-warnings");

ignorer.default("error", [
    'The fs.promises API is experimenta'
])

module.exports = fs.promises;

const Util = require("util");
module.exports.path = require("path");
module.exports.fs = fs;

const mkdir = Util.promisify(fs.mkdir);

/**
 * Check if pathName exists
 * 
 * @param {String} pathName path to check if exists
 * @returns {Boolean} True if exists false if not
 */
module.exports.exists = async function (pathName) {
    return new Promise(r => fs.access(pathName, fs.F_OK, e => r(!e)))
}

/**
 * Make dir async and check if exists to prevent create existing dir
 * 
 * @param {String} pathName full path to create
 * @param {{}} opts options to native fs.mdir, defaults {recursive: true} 
 */
module.exports.mkdir = async function (pathName, opts = { recursive: true }) {
    if (!await this.exists(pathName)) {
        await mkdir(pathName, opts);
    }
}

/**
 * Walk inside directories recursively and returns an async generator with path files
 * 
 * @param {String} dir Path to walk recursive
 * @param {String[]} ignore regex patters to ignore defaults [/\.git/]
 * @returns {String[]} Found files as generator
 */
module.exports.walk = async function* (dir, ignore = [/\.git/]) {
    const files = await this.readdir(dir)
    for (const file of files) {
        const stat = await this.lstat(this.path.join(dir, file))
        if (stat.isSymbolicLink() || ignore.filter(x => x.test(file)).length > 0 ) {
            continue;
        }
        else if (stat.isDirectory()) {
            for await (const inner of this.walk(this.path.join(dir, file), ignore)) {
                yield inner;
            }
        }
        else if (stat.isFile()) {
            yield this.path.join(dir, file)
        }
    }
}

module.exports.sync = {}

for (const func in fs){
    if(~func.indexOf("Sync")){
        module.exports.sync[func.replace("Sync", "")] = fs[func];
    }
}